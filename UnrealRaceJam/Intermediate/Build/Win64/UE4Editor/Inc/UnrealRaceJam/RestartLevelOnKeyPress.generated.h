// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREALRACEJAM_RestartLevelOnKeyPress_generated_h
#error "RestartLevelOnKeyPress.generated.h already included, missing '#pragma once' in RestartLevelOnKeyPress.h"
#endif
#define UNREALRACEJAM_RestartLevelOnKeyPress_generated_h

#define UnrealRaceJam_Source_UnrealRaceJam_GameManagement_RestartLevelOnKeyPress_h_15_SPARSE_DATA
#define UnrealRaceJam_Source_UnrealRaceJam_GameManagement_RestartLevelOnKeyPress_h_15_RPC_WRAPPERS
#define UnrealRaceJam_Source_UnrealRaceJam_GameManagement_RestartLevelOnKeyPress_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define UnrealRaceJam_Source_UnrealRaceJam_GameManagement_RestartLevelOnKeyPress_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARestartLevelOnKeyPress(); \
	friend struct Z_Construct_UClass_ARestartLevelOnKeyPress_Statics; \
public: \
	DECLARE_CLASS(ARestartLevelOnKeyPress, AGameStateBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealRaceJam"), NO_API) \
	DECLARE_SERIALIZER(ARestartLevelOnKeyPress)


#define UnrealRaceJam_Source_UnrealRaceJam_GameManagement_RestartLevelOnKeyPress_h_15_INCLASS \
private: \
	static void StaticRegisterNativesARestartLevelOnKeyPress(); \
	friend struct Z_Construct_UClass_ARestartLevelOnKeyPress_Statics; \
public: \
	DECLARE_CLASS(ARestartLevelOnKeyPress, AGameStateBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealRaceJam"), NO_API) \
	DECLARE_SERIALIZER(ARestartLevelOnKeyPress)


#define UnrealRaceJam_Source_UnrealRaceJam_GameManagement_RestartLevelOnKeyPress_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARestartLevelOnKeyPress(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARestartLevelOnKeyPress) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARestartLevelOnKeyPress); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARestartLevelOnKeyPress); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARestartLevelOnKeyPress(ARestartLevelOnKeyPress&&); \
	NO_API ARestartLevelOnKeyPress(const ARestartLevelOnKeyPress&); \
public:


#define UnrealRaceJam_Source_UnrealRaceJam_GameManagement_RestartLevelOnKeyPress_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARestartLevelOnKeyPress(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARestartLevelOnKeyPress(ARestartLevelOnKeyPress&&); \
	NO_API ARestartLevelOnKeyPress(const ARestartLevelOnKeyPress&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARestartLevelOnKeyPress); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARestartLevelOnKeyPress); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARestartLevelOnKeyPress)


#define UnrealRaceJam_Source_UnrealRaceJam_GameManagement_RestartLevelOnKeyPress_h_15_PRIVATE_PROPERTY_OFFSET
#define UnrealRaceJam_Source_UnrealRaceJam_GameManagement_RestartLevelOnKeyPress_h_12_PROLOG
#define UnrealRaceJam_Source_UnrealRaceJam_GameManagement_RestartLevelOnKeyPress_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealRaceJam_Source_UnrealRaceJam_GameManagement_RestartLevelOnKeyPress_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealRaceJam_Source_UnrealRaceJam_GameManagement_RestartLevelOnKeyPress_h_15_SPARSE_DATA \
	UnrealRaceJam_Source_UnrealRaceJam_GameManagement_RestartLevelOnKeyPress_h_15_RPC_WRAPPERS \
	UnrealRaceJam_Source_UnrealRaceJam_GameManagement_RestartLevelOnKeyPress_h_15_INCLASS \
	UnrealRaceJam_Source_UnrealRaceJam_GameManagement_RestartLevelOnKeyPress_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealRaceJam_Source_UnrealRaceJam_GameManagement_RestartLevelOnKeyPress_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealRaceJam_Source_UnrealRaceJam_GameManagement_RestartLevelOnKeyPress_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealRaceJam_Source_UnrealRaceJam_GameManagement_RestartLevelOnKeyPress_h_15_SPARSE_DATA \
	UnrealRaceJam_Source_UnrealRaceJam_GameManagement_RestartLevelOnKeyPress_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	UnrealRaceJam_Source_UnrealRaceJam_GameManagement_RestartLevelOnKeyPress_h_15_INCLASS_NO_PURE_DECLS \
	UnrealRaceJam_Source_UnrealRaceJam_GameManagement_RestartLevelOnKeyPress_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREALRACEJAM_API UClass* StaticClass<class ARestartLevelOnKeyPress>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UnrealRaceJam_Source_UnrealRaceJam_GameManagement_RestartLevelOnKeyPress_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
