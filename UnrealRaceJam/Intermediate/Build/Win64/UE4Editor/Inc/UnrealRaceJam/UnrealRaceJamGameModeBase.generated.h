// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREALRACEJAM_UnrealRaceJamGameModeBase_generated_h
#error "UnrealRaceJamGameModeBase.generated.h already included, missing '#pragma once' in UnrealRaceJamGameModeBase.h"
#endif
#define UNREALRACEJAM_UnrealRaceJamGameModeBase_generated_h

#define UnrealRaceJam_Source_UnrealRaceJam_UnrealRaceJamGameModeBase_h_15_SPARSE_DATA
#define UnrealRaceJam_Source_UnrealRaceJam_UnrealRaceJamGameModeBase_h_15_RPC_WRAPPERS
#define UnrealRaceJam_Source_UnrealRaceJam_UnrealRaceJamGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define UnrealRaceJam_Source_UnrealRaceJam_UnrealRaceJamGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAUnrealRaceJamGameModeBase(); \
	friend struct Z_Construct_UClass_AUnrealRaceJamGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AUnrealRaceJamGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealRaceJam"), NO_API) \
	DECLARE_SERIALIZER(AUnrealRaceJamGameModeBase)


#define UnrealRaceJam_Source_UnrealRaceJam_UnrealRaceJamGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAUnrealRaceJamGameModeBase(); \
	friend struct Z_Construct_UClass_AUnrealRaceJamGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AUnrealRaceJamGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/UnrealRaceJam"), NO_API) \
	DECLARE_SERIALIZER(AUnrealRaceJamGameModeBase)


#define UnrealRaceJam_Source_UnrealRaceJam_UnrealRaceJamGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUnrealRaceJamGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUnrealRaceJamGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUnrealRaceJamGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUnrealRaceJamGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUnrealRaceJamGameModeBase(AUnrealRaceJamGameModeBase&&); \
	NO_API AUnrealRaceJamGameModeBase(const AUnrealRaceJamGameModeBase&); \
public:


#define UnrealRaceJam_Source_UnrealRaceJam_UnrealRaceJamGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AUnrealRaceJamGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AUnrealRaceJamGameModeBase(AUnrealRaceJamGameModeBase&&); \
	NO_API AUnrealRaceJamGameModeBase(const AUnrealRaceJamGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AUnrealRaceJamGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AUnrealRaceJamGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AUnrealRaceJamGameModeBase)


#define UnrealRaceJam_Source_UnrealRaceJam_UnrealRaceJamGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define UnrealRaceJam_Source_UnrealRaceJam_UnrealRaceJamGameModeBase_h_12_PROLOG
#define UnrealRaceJam_Source_UnrealRaceJam_UnrealRaceJamGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealRaceJam_Source_UnrealRaceJam_UnrealRaceJamGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealRaceJam_Source_UnrealRaceJam_UnrealRaceJamGameModeBase_h_15_SPARSE_DATA \
	UnrealRaceJam_Source_UnrealRaceJam_UnrealRaceJamGameModeBase_h_15_RPC_WRAPPERS \
	UnrealRaceJam_Source_UnrealRaceJam_UnrealRaceJamGameModeBase_h_15_INCLASS \
	UnrealRaceJam_Source_UnrealRaceJam_UnrealRaceJamGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define UnrealRaceJam_Source_UnrealRaceJam_UnrealRaceJamGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	UnrealRaceJam_Source_UnrealRaceJam_UnrealRaceJamGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	UnrealRaceJam_Source_UnrealRaceJam_UnrealRaceJamGameModeBase_h_15_SPARSE_DATA \
	UnrealRaceJam_Source_UnrealRaceJam_UnrealRaceJamGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	UnrealRaceJam_Source_UnrealRaceJam_UnrealRaceJamGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	UnrealRaceJam_Source_UnrealRaceJam_UnrealRaceJamGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UNREALRACEJAM_API UClass* StaticClass<class AUnrealRaceJamGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID UnrealRaceJam_Source_UnrealRaceJam_UnrealRaceJamGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
