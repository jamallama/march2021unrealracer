// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UnrealRaceJam/GameManagement/RestartLevelOnKeyPress.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRestartLevelOnKeyPress() {}
// Cross Module References
	UNREALRACEJAM_API UClass* Z_Construct_UClass_ARestartLevelOnKeyPress_NoRegister();
	UNREALRACEJAM_API UClass* Z_Construct_UClass_ARestartLevelOnKeyPress();
	ENGINE_API UClass* Z_Construct_UClass_AGameStateBase();
	UPackage* Z_Construct_UPackage__Script_UnrealRaceJam();
// End Cross Module References
	void ARestartLevelOnKeyPress::StaticRegisterNativesARestartLevelOnKeyPress()
	{
	}
	UClass* Z_Construct_UClass_ARestartLevelOnKeyPress_NoRegister()
	{
		return ARestartLevelOnKeyPress::StaticClass();
	}
	struct Z_Construct_UClass_ARestartLevelOnKeyPress_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ARestartLevelOnKeyPress_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameStateBase,
		(UObject* (*)())Z_Construct_UPackage__Script_UnrealRaceJam,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARestartLevelOnKeyPress_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "GameManagement/RestartLevelOnKeyPress.h" },
		{ "ModuleRelativePath", "GameManagement/RestartLevelOnKeyPress.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ARestartLevelOnKeyPress_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ARestartLevelOnKeyPress>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ARestartLevelOnKeyPress_Statics::ClassParams = {
		&ARestartLevelOnKeyPress::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002A4u,
		METADATA_PARAMS(Z_Construct_UClass_ARestartLevelOnKeyPress_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ARestartLevelOnKeyPress_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ARestartLevelOnKeyPress()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ARestartLevelOnKeyPress_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ARestartLevelOnKeyPress, 256099014);
	template<> UNREALRACEJAM_API UClass* StaticClass<ARestartLevelOnKeyPress>()
	{
		return ARestartLevelOnKeyPress::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ARestartLevelOnKeyPress(Z_Construct_UClass_ARestartLevelOnKeyPress, &ARestartLevelOnKeyPress::StaticClass, TEXT("/Script/UnrealRaceJam"), TEXT("ARestartLevelOnKeyPress"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ARestartLevelOnKeyPress);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
