// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UnrealRaceJamGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UNREALRACEJAM_API AUnrealRaceJamGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
